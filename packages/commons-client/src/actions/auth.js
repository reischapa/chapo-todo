import * as actionTypes from '../action-types';
import {sendAuthenticatedRequest, sendRequest} from "../utils";
import {ImplementationRegistry} from "../implementation-registry";

export function setEmail(email) {
  return {
    type: actionTypes.SET_EMAIL,
    payload: {
      email
    }
  }
}

export function setPassword(password) {
  return {
    type: actionTypes.SET_PASSWORD,
    payload: {
      password
    }
  }
}

export function login(email, password) {
  return async dispatch => {
    try {
      const result = await sendRequest({
        url: 'http://localhost:8000/login',
        method: 'post',
        body: {email, password}
      })

      const {token, user} = result.data;

      dispatch(setFailedAuthenticatedRequestFlag(false));
      dispatch(setAuthInfo(token, user));

    } catch (e) {
      console.log(e);
    }
  }
}


export function setAuthInfo(token, user) {
  return async dispatch => {
    await ImplementationRegistry.getImplementation('saveToPersistentStorage')({key: 'token', data: token});
    await ImplementationRegistry.getImplementation('saveToPersistentStorage')({key: 'user', data: user});

    dispatch({
      type: actionTypes.SET_AUTH_INFO,
      payload: {
        token,
        user
      }
    });
  }
}

export function logout() {
  return async (dispatch, getState) => {
    try {
      await sendAuthenticatedRequest({
        url: 'http://localhost:8000/logout',
        method: 'post',
        state: getState()
      })

      dispatch(setAuthInfo(null, null));

    } catch (e) {
      console.log(e);
    }
  }
}

export function setFailedAuthenticatedRequestFlag(failedAuthenticatedRequestFlag) {
  return {
    type: actionTypes.SET_FAILED_AUTHENTICATED_REQUEST_FLAG,
    payload: {
      failedAuthenticatedRequestFlag
    }
  }
}
