import * as actionTypes from '../action-types'
import {selectedChecklistIdSelector, userIdSelector} from "../selectors";
import {sendAuthenticatedRequest} from "../utils";
import {setAuthInfo, setFailedAuthenticatedRequestFlag} from "./auth";

export function setSelectedChecklistTasks(selectedChecklistTasks) {
  return {
    type: actionTypes.SET_SELECTED_CHECKLIST_TASKS,
    payload: {
      selectedChecklistTasks
    }
  }
}

export function fetchSelectedChecklistTasks() {
  return async (dispatch, getState) => {
    const selectedChecklistId = selectedChecklistIdSelector(getState());
    const userId = userIdSelector(getState());

    try {
      const result = await sendAuthenticatedRequest({
        url: `http://localhost:8000/users/${userId}/checklists/${selectedChecklistId}/checklist-tasks`,
        state: getState()
      })

      dispatch(setSelectedChecklistTasks(result.data.checklistTasks));
    } catch (e) {
      if (e.response.status == 401) {
        dispatch(setFailedAuthenticatedRequestFlag(true));
        dispatch(setAuthInfo(null, null));
      }

      console.log(e)
    }
  }
}

export function createSelectedChecklistTask(body) {
  return async (dispatch, getState) => {
    const selectedChecklistId = selectedChecklistIdSelector(getState());
    const userId = userIdSelector(getState());

    try {
      await sendAuthenticatedRequest({
        method: 'post',
        url: `http://localhost:8000/users/${userId}/checklists/${selectedChecklistId}/checklist-tasks`,
        state: getState(),
        body
      })

      dispatch(fetchSelectedChecklistTasks());
    } catch (e) {
      if (e.response.status == 401) {
        dispatch(setFailedAuthenticatedRequestFlag(true));
        dispatch(setAuthInfo(null, null));
      }

      console.log(e)
    }
  }
}

export function deleteChecklistTask(checklistTaskId) {
  return async (dispatch, getState) => {
    const selectedChecklistId = selectedChecklistIdSelector(getState());
    const userId = userIdSelector(getState());

    try {
      await sendAuthenticatedRequest({
        method: 'delete',
        url: `http://localhost:8000/users/${userId}/checklists/${selectedChecklistId}/checklist-tasks/${checklistTaskId}`,
        state: getState(),
      })

      dispatch(fetchSelectedChecklistTasks());
    } catch (e) {
      if (e.response.status == 401) {
        dispatch(setFailedAuthenticatedRequestFlag(true));
        dispatch(setAuthInfo(null, null));
      }

      console.log(e)
    }
  }
}

