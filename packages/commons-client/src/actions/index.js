import * as actionTypes from '../action-types';
import {sendAuthenticatedRequest, sendRequest} from "../utils";
import {userIdSelector} from "../selectors";
import {setAuthInfo, setFailedAuthenticatedRequestFlag} from "./auth";

export * from './auth';
export * from './checklist-tasks';

export function getChecklistList() {
  return async (dispatch, getState) => {
    try {
      const result = await sendAuthenticatedRequest({
        url: `http://localhost:8000/users/${userIdSelector(getState())}/checklists`,
        state: getState()
      })

      dispatch(setChecklistList(result.data.checklistList));
    } catch (e) {
      if (e.response.status == 401) {
        dispatch(setFailedAuthenticatedRequestFlag(true));
        dispatch(setAuthInfo(null, null));
      }

      console.log(e);
    }
  }
}

export function setChecklistList(checklistList) {
  return {
    type: actionTypes.SET_CHECKLIST_LIST,
    payload: {
      checklistList
    }
  }
}

export function setSelectedChecklistId(selectedChecklistId) {
  return {
    type: actionTypes.SET_SELECTED_CHECKLIST_ID,
    payload: {
      selectedChecklistId
    }
  }
}

export function saveChecklist(checklist) {
  return async (dispatch, getState) => {
    try {
      await sendAuthenticatedRequest({
        url: `http://localhost:8000/users/${userIdSelector(getState())}/checklists`,
        method: 'post',
        body: checklist,
        state: getState()
      })

      dispatch(getChecklistList());
    } catch (e) {
      if (e.response.status == 401) {
        dispatch(setFailedAuthenticatedRequestFlag(true));
        dispatch(setAuthInfo(null, null));
      }

      console.log(e)
    }
  }
}

export function deleteChecklist(checklistId) {
  return async (dispatch, getState) => {
    const result = confirm('Are you sure?');

    if (!result) {
      return;
    }

    try {
      await sendAuthenticatedRequest({
        url: `http://localhost:8000/users/${userIdSelector(getState())}/checklists/${checklistId}`,
        method: 'delete',
        state: getState()
      })

      dispatch(getChecklistList());

    } catch (e) {

      if (e.response.status == 401) {
        dispatch(setFailedAuthenticatedRequestFlag(true));
        dispatch(setAuthInfo(null, null));
      }
      console.log(e);
    }

  };
}