import {useEffect} from 'react';
import {useSelector} from 'react-redux';
import {failedAuthenticationRequestFlagSelector, tokenSelector} from "./selectors";
import {ImplementationRegistry} from "./implementation-registry";

export function useRedirectIfNotLoggedIn() {
  const token = useSelector(tokenSelector);
  const failedAuthenticationRequestFlag = useSelector(failedAuthenticationRequestFlagSelector);
  const navigate = ImplementationRegistry.getImplementation('getNavigateHook')();

  useEffect(() => {
    if (!token || failedAuthenticationRequestFlag) {
      navigate({target: 'Login'});
    }
  }, [token, failedAuthenticationRequestFlag]);

}

export function useRedirectIfLoggedIn() {
  const token = useSelector(tokenSelector);
  const failedAuthenticationRequestFlag = useSelector(failedAuthenticationRequestFlagSelector);
  const navigate = ImplementationRegistry.getImplementation('getNavigateHook')();

  useEffect(() => {
    if (token && !failedAuthenticationRequestFlag) {
      navigate({target: 'Index'});
    }
  }, [token, failedAuthenticationRequestFlag]);
}