class ImplementationRegistryClass {
  constructor() {
    this.implementationMap = new Map();
  }

  registerImplementation(key, impl) {
    this.implementationMap.set(key, impl);
  }

  getImplementation(key) {
    if (!this.implementationMap.has(key)) {
      throw new Error(`Implementation for ${key} has not been set yet!`)
    }

    return this.implementationMap.get(key);
  }

}

export const ImplementationRegistry = new ImplementationRegistryClass();

