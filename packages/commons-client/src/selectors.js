import * as _ from 'lodash';

export const emailSelector = state => state.email;
export const passwordSelector = state => state.password;
export const tokenSelector = state => state.token;
export const userSelector = state => state.user;
export const userIdSelector = state => _.get(userSelector(state), '_id');

export const checklistListSelector = state => _.get(state, 'checklistList', []);
export const selectedChecklistIdSelector = state => _.get(state, 'selectedChecklistId');
export const selectedChecklistTasksSelector = state => _.get(state, 'selectedChecklistTasks', []);

export const selectedChecklistTaskSelector = state => {
  const id = selectedChecklistIdSelector(state);
  return checklistListSelector(state).find(e => e._id === id);
}

export const failedAuthenticationRequestFlagSelector = state => state.failedAuthenticatedRequestFlag;