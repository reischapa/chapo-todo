import axios from 'axios';
import {tokenSelector} from "./selectors";
import {ImplementationRegistry} from "./implementation-registry";

const keysSavedInPersistentStorage = ['user', 'token'];

export async function loadInitialStateFromPersistentStorage() {
  const initialState = {};

  for (const key of keysSavedInPersistentStorage) {
    initialState[key] = await ImplementationRegistry.getImplementation('loadFromPersistentStorage')({key});
  }

  console.log(888, initialState)

  return initialState;
}

export async function sendRequest({url, body, method = 'get', query = {}, headers = {}}) {
  const finalHeaders = {
    ...headers,
    'Content-Type': "application/json",
  }

  const result = await axios({url, method,  data: body, headers: finalHeaders, params: query});

  return result.data;
}

export async function sendAuthenticatedRequest({url, body, method = 'get', query = {}, headers = {}, state}) {
  const finalHeaders = {
    ...headers,
    'Authorization': `Bearer ${tokenSelector(state).token}`
  }

  return sendRequest({
    url,
    body,
    method,
    query,
    headers: finalHeaders
  })
}