import {ImplementationRegistry} from "commons-client";

export function setupImplementationRegistry() {
  ImplementationRegistry.registerImplementation('getNavigateHook', function(props) {
    return function({target}) {
      props.navigation.navigate(target)
    }
  });


}