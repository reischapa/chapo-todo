import React from 'react';
import 'react-native-gesture-handler';
import { registerRootComponent } from 'expo';

import {setupImplementationRegistry} from "./implementation-registry-setup";
import {loadInitialStateFromPersistentStorage} from "commons-client";
import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import {Provider} from "react-redux";
import {NavigationContainer} from "@react-navigation/native";
import {Login} from "./components/Login";

setupImplementationRegistry();

Promise.resolve({}).then(initialState => {
  function reducer(state = initialState, action) {
    return {
      ...state,
      ...action.payload
    }
  }

  const store = createStore(reducer, applyMiddleware(thunk));

  const Stack = createNativeStackNavigator();

  function App() {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name="Login" component={Login} />
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    );
  }

  registerRootComponent(() => {
    return (
      <App/>
    );
  });
})

