const createExpoWebpackConfigAsync = require('@expo/webpack-config');

module.exports = async function (env, argv) {
  const config = await createExpoWebpackConfigAsync(env, argv);

  let javascriptRule = config.module.rules[1].oneOf[2];

  let originalFn = javascriptRule.include;

  javascriptRule.include = function(path) {
    return /commons-client/.test(path) || originalFn(path);
  }

  return config;
};
