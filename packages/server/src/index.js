const {initMongoose} = require("./init-mongoose");
const {initServer} = require("./init-server");

async function init() {
  await initMongoose();
  await initServer();
}

init().catch(e => console.log(e))
