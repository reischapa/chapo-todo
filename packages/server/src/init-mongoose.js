const mongoose = require('mongoose');

module.exports.initMongoose = async function initMongoose() {
  await new Promise(res => {
    mongoose.connect('mongodb://localhost:27017/chapo-todo');

    mongoose.connection.on('connected', () => {
      console.log('Connected to mongoose');
      res();
    })
  })
}