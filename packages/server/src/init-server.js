const express = require('express')
const cors = require('cors');
const {addRoutes} = require("./routes");

const app = express();

app.use(express.json());
app.use(cors())

const port = 8000;

addRoutes(app);

module.exports.initServer = async function initServer() {
  await new Promise(res => {
    app.listen(port, () => {
      console.log(`Listening on port ${port}`);
      res();
    })
  });
}
