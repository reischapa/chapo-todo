const mongoose = require('mongoose');
const {generateToken} = require("./utils");

const UserModel = mongoose.model('User', new mongoose.Schema({
  email: String,
  passwordHash: String,
  passwordSalt: String,
  deleted: {type: Boolean, default: false}
}, {timestamps: true}))

const UserTokenModel = mongoose.model('UserToken', new mongoose.Schema({
  userId: String,
  token: {type: String, default: () => generateToken()},
  loginType: {type: String, default: 'userPassword'},
  state: {type: String, default: 'created'},
  deleted: {type: Boolean, default: false}
}, {timestamps: true}))

const ChecklistModel = mongoose.model('Checklist', new mongoose.Schema({
  userId: String,
  title: String,
  deleted: {type: Boolean, default: false}
}));

const ChecklistTaskModel = mongoose.model('ChecklistTask', new mongoose.Schema({
  userId: String,
  checklistId: String,
  title: String,
  isCompleted: Boolean,
  deleted: {type: Boolean, default: false}
}));

module.exports = {
  UserModel,
  UserTokenModel,
  ChecklistModel,
  ChecklistTaskModel
}