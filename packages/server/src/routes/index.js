const QRCode = require("qrcode");
const {UserModel, UserTokenModel, ChecklistModel, ChecklistTaskModel} = require('../models');
const {calculateStringHash, safeTimingStringCompare, generateSalt} = require('../utils');
const _ = require('lodash');

const loginRouteHandler = async (req, res, next) => {
  const body = req.body;

  const user = await UserModel.findOne({email: body.email, deleted: false});

  if (!user) {
    res.send(401);
    return next();
  }

  const hash = calculateStringHash(body.password, user.passwordSalt);

  if (!safeTimingStringCompare(hash, user.passwordHash)) {
    res.send(401);
    return next();
  }

  let sentToken;
  const existingToken = await UserTokenModel.findOne({userId: user._id, loginType: 'userPassword', deleted: false});

  if (existingToken) {
    sentToken = existingToken;
  } else {
    sentToken = await UserTokenModel.create({userId: user._id, loginType: 'userPassword'})
  }

  res.send({
    data: {
      token: sentToken.toJSON(),
      user: _.omit(user.toJSON(), ['passwordHash', 'passwordSalt'])
    }
  });
}

const logoutRouteHandler = async (req, res, next) => {
  const {token} = req.locals;

  await UserTokenModel.remove({_id: token._id});

  res.send({});
}

const requestLoginQrCodeRouteHandler = async (req, res, next) => {
  const {token} = req.locals;

  if (token.loginType !== 'userPassword') {
    res.send(400);
    return next();
  }

  const newToken = await UserTokenModel.create({userId: token.userId, state: 'confirming'});

  const qrCodeDataUri = await new Promise((resolve, reject) => {
    QRCode.toDataURL(newToken.token, (err, string) => {
      if (err) reject(err)
      resolve(string);
    });
  });

  res.send({
    data: {
      qrCodeDataUri
    }
  })
}

const confirmLoginQrCodeRouteHandler = async (req, res, next) => {
  const {token} = req.locals;

  if (token.loginType !== 'qrCode' && token.state !== 'confirming') {
    res.send(400);
    return next();
  }

  await UserTokenModel.updateOne({_id: token._id}, {state: 'confirmed'});

  res.send({})
  return next();
}

const createUserRouteHandler = async (req, res) => {
  const {body} = req;

  const passwordSalt = await generateSalt();
  const passwordHash = calculateStringHash(body.password, passwordSalt);

  await UserModel.create({
    email: body.email,
    passwordSalt,
    passwordHash
  });

  res.send(201);
}

const createChecklistRouteHandler = async (req, res) => {
  const {body, params} = req;

  const createdChecklist = await ChecklistModel.create({
    ...body,
    userId: params.userId
  });

  res.status(201).send(createdChecklist);
}

const getChecklistsForUserRouteHandler = async (req, res) => {
  const {params} = req;
  const {user} = req.locals;

  if (user._id.toString() !== params.userId) {
    res.status(401).send('Unauthorized');
    return;
  }

  const checklistList = await ChecklistModel.find({userId: params.userId, deleted: false});

  res.send({
    data: {
      checklistList
    }
  })
}

const deleteChecklistForUserRouteHandler = async (req, res) => {
  const {params} = req;
  const {user} = req.locals;

  if (user._id.toString() !== params.userId) {
    res.status(401).send('Unauthorized');
    return;
  }

  await ChecklistModel.updateOne({_id: params.checklistId, userId: params.userId, deleted: false}, {deleted: true});

  res.status(201);

  res.send({
    data: {}
  })
}

const getChecklistTasksRouteHandler = async (req, res) => {
  const {params} = req;
  const {user} = req.locals;

  if (user._id.toString() !== params.userId) {
    res.status(401).send('Unauthorized');
    return;
  }

  const checklist = await ChecklistModel.findOne({_id: params.checklistId, userId: params.userId, deleted: false});

  if (!checklist) {
    res.status(400).send('Checklist does not exist');
  }

  const checklistTasks = await ChecklistTaskModel.find({checklistId: params.checklistId, userId: params.userId, deleted: false});

  res.status(200);

  res.send({
    data: {
      checklistTasks
    }
  })
}

const createChecklistTaskRouteHandler = async (req, res) => {
  const {params, body} = req;
  const {user} = req.locals;

  if (user._id.toString() !== params.userId) {
    res.status(401).send('Unauthorized');
    return;
  }

  const checklist = await ChecklistModel.findOne({_id: params.checklistId, userId: params.userId, deleted: false});

  if (!checklist) {
    res.status(400).send('Checklist does not exist');
  }

  await ChecklistTaskModel.create({
    checklistId: checklist._id,
    userId: checklist.userId,
    ...body
  })

  res.status(201);
  res.send({data: {}});
}

const deleteChecklistTaskRouteHandler = async (req, res) => {
  const {params} = req;
  const {user} = req.locals;

  if (user._id.toString() !== params.userId) {
    res.status(401).send('Unauthorized');
    return;
  }

  const checklist = await ChecklistModel.findOne({_id: params.checklistId, userId: params.userId, deleted: false});

  if (!checklist) {
    res.status(400).send('Checklist does not exist');
  }

  await ChecklistTaskModel.updateOne({_id: params.checklistTaskId}, {deleted: true});

  res.send({data: {}});
}

const routeDefinitions = {
  loginRouteDefinition: {
    method: 'post',
    path: '/login',
    useAuthMiddleware: false,
    handler: loginRouteHandler
  },
  logoutRouteDefinition: {
    method: 'post',
    path: '/logout',
    handler: logoutRouteHandler
  },
  requestLoginQrCodeRouteDefinition: {
    method: 'post',
    path: '/request-qr-code-login',
    handler: requestLoginQrCodeRouteHandler
  },
  confirmLoginQrCodeRouteDefinition: {
    method: 'post',
    path: '/confirm-qr-code-login',
    handler: confirmLoginQrCodeRouteHandler
  },
  createUserRouteDefinition: {
    method: 'post',
    path: '/users',
    useAuthMiddleware: false,
    handler: createUserRouteHandler
  },
  getChecklistsForUserRouteDefinition: {
    method: 'get',
    path: '/users/:userId/checklists',
    handler: getChecklistsForUserRouteHandler
  },
  createChecklistRouteDefinition: {
    method: 'post',
    path: '/users/:userId/checklists',
    handler: createChecklistRouteHandler
  },
  deleteChecklistForUserRouteDefinition: {
    method: 'delete',
    path: '/users/:userId/checklists/:checklistId',
    handler: deleteChecklistForUserRouteHandler
  },
  getChecklistTasksRouteDefinition: {
    method: 'get',
    path: '/users/:userId/checklists/:checklistId/checklist-tasks',
    handler: getChecklistTasksRouteHandler
  },
  createChecklistTaskRouteDefinition: {
    method: 'post',
    path: '/users/:userId/checklists/:checklistId/checklist-tasks',
    handler: createChecklistTaskRouteHandler
  },
  deleteChecklistTaskRoute: {
    method: 'delete',
    path: '/users/:userId/checklists/:checklistId/checklist-tasks/:checklistTaskId',
    handler: deleteChecklistTaskRouteHandler
  }
}

async function authMiddleware(req, res, next) {
  const tokenString = req.headers['authorization'];

  if (!tokenString) {
    res.status(401);
    res.send('Unauthorized');
    return;
  }

  const processedTokenString = tokenString.replace('Bearer ', '');

  console.log(processedTokenString)

  const existingToken = await UserTokenModel.findOne({token: processedTokenString, deleted: false});

  if (!existingToken) {
    res.status(401);
    res.send('Unauthorized');
    return;
  }

  const user = await UserModel.findOne({_id: existingToken.userId, deleted: false});

  _.set(req, 'locals.token', existingToken.toJSON());
  _.set(req, 'locals.user', user.toJSON());

  return next();
}

function addRoutes(app) {
  Object.keys(routeDefinitions).forEach(routeDefinitionName => {
    const routeDefinition = routeDefinitions[routeDefinitionName]
    const {method, path, handler, useAuthMiddleware = true} = routeDefinition;

    if (useAuthMiddleware) {
      app[method](path, authMiddleware);
    }

    app[method](path, async (req, res, next) => {
      try {
        await handler(req, res, next);
      } catch (e) {
        next(e);
      }
    })
  })
}

module.exports = {
  addRoutes
}
