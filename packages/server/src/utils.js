const crypto = require('crypto');
const uuid = require('uuid');

async function generateRandomString(len) {
  return new Promise((res, rej) => {
    crypto.randomBytes(len, function (err, buffer) {
      if (err) rej(err);
      res(buffer.toString('hex'));
    })
  })
}

const generateSalt = () => generateRandomString(24);
const generateToken = () => uuid.v4();

function calculateStringHash(string, salt = '') {
  return crypto.createHash('sha256').update(`${string}${salt}`).digest('base64');
}

function safeTimingStringCompare(a, b) {
  return crypto.timingSafeEqual(Buffer.from(a), Buffer.from(b));
}

module.exports = {
  generateSalt,
  generateToken,
  calculateStringHash,
  safeTimingStringCompare
}
