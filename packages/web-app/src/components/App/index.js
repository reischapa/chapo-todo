import React from 'react';
import {ChecklistList} from "../ChecklistList";
import {ChecklistCreationForm} from "../ChecklistCreationForm";

import {useRedirectIfNotLoggedIn} from 'commons-client/src/hooks';

import './index.css';
import {SelectedChecklistDetails} from "../SelectedChecklistDetails";
import {Header} from "../Header";

export function App(props) {
  useRedirectIfNotLoggedIn(props);

  return (
    <div className={'app'}>
      <Header/>
      <div className={'app-container'}>
        <div className={'checklist-list-container'}>
          <span className="title">Checklists</span>
          <ChecklistList/>
          <ChecklistCreationForm/>
        </div>
        <div className={'checklist-details-container'}>
          <SelectedChecklistDetails/>
        </div>
      </div>
    </div>
  );
}