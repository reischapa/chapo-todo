import React from 'react';
import {useState} from 'react';
import {useDispatch} from 'react-redux';

import {saveChecklist} from "commons-client/src/actions";

import './index.css';

export function ChecklistCreationForm() {
  const [title, setTitle] = useState();
  const dispatch = useDispatch();

  const onSubmit = evt => {
    evt.preventDefault();

    dispatch(saveChecklist({
      title
    }));
  }

  return (
    <div className={'checklist-creation-form'}>
      <form onSubmit={onSubmit}>
        <span>New Checklist:</span>
        <input value={title} onChange={evt => setTitle(evt.target.value)}/>
        <button type="submit">Create</button>
      </form>
    </div>
  )

}