import React from 'react';
import {useEffect} from 'react';
import {useSelector, useDispatch} from "react-redux";

import {checklistListSelector} from "commons-client/src/selectors";
import {getChecklistList} from "commons-client/src/actions";
import {ChecklistListElement} from "../ChecklistListElement";

import './index.css';

export function ChecklistList() {
  const checklistList = useSelector(checklistListSelector);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getChecklistList());
  }, []);

  return (
    <div className={'checklist-list'}>
      {checklistList.map(checklist => <ChecklistListElement checklist={checklist}/>)}
    </div>
  )
}