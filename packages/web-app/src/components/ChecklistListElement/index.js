import {useDispatch, useSelector} from 'react-redux';
import './index.css';

import {deleteChecklist, selectedChecklistIdSelector, setSelectedChecklistId} from 'commons-client';

export function ChecklistListElement(props) {
  const {checklist} = props;
  const {_id, title} = checklist;
  const selectedChecklistId = useSelector(selectedChecklistIdSelector);
  const dispatch = useDispatch();

  const onDivClick = evt => {
    evt.preventDefault();

    dispatch(setSelectedChecklistId(_id));
  }

  const onButtonClick = evt => {
    evt.preventDefault();
    evt.stopPropagation();

    dispatch(deleteChecklist(_id))
  }

  const isSelectedChecklist = selectedChecklistId === checklist._id;
  const classNames = `checklist-list-element ${isSelectedChecklist ? 'selected' : ''}`

  return (
    <div className={classNames} onClick={onDivClick}>
      <p>{title}</p>
      <button onClick={onButtonClick}>Delete</button>
    </div>
  )
}