import {useDispatch} from 'react-redux';
import './index.css';
import {deleteChecklistTask} from "commons-client";

export function ChecklistTask(props) {
  const {checklistTask, checked, setChecked} = props;
  const {title} = checklistTask;
  const dispatch = useDispatch();

  const onDeleteButtonClick = evt => {
    evt.preventDefault();
    evt.stopPropagation();

    dispatch(deleteChecklistTask(checklistTask._id));
  }

  return (
    <div className={'checklist-task'} onClick={() => setChecked(!checked)}>
      <input type="checkbox" checked={checked} onChange={evt => setChecked(evt.target.checked)}/>
      <div className="title">
        <span>{title}</span>
      </div>
      <button type="submit" onClick={onDeleteButtonClick}>Delete</button>
    </div>
  )
}