import React, {useEffect} from "react";
import {ImplementationRegistry} from "commons-client";

import './index.css';

export function Default() {
  const navigate = ImplementationRegistry.getImplementation('getNavigateHook')();

  useEffect(() => navigate({target: "Index"}), []);

  return <div/>;
}