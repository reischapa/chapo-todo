import {useDispatch} from 'react-redux';
import {logout} from "commons-client/src/actions";

import './index.css';

export function Header() {
  const dispatch = useDispatch();

  return (
    <div className={'header'}>
      <button onClick={() => dispatch(logout())}>Logout</button>
    </div>
  )
}