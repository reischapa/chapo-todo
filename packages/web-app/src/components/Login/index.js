import React from 'react';
import {useSelector, useDispatch} from 'react-redux';

import {login, setEmail, setPassword} from "commons-client/src/actions";
import {useRedirectIfLoggedIn} from "commons-client/src/hooks";
import {emailSelector, passwordSelector} from "commons-client/src/selectors";

import './index.css';

export function Login() {
  const email = useSelector(emailSelector);
  const password = useSelector(passwordSelector);
  const dispatch = useDispatch();

  useRedirectIfLoggedIn();

  const onFormSubmit = async evt => {
    evt.preventDefault();
    dispatch(login(email, password));
  }

  return (
    <div className={'login'}>
      <form onSubmit={onFormSubmit}>
        <span>User</span>
        <input type="text" value={email} onChange={evt => dispatch(setEmail(evt.target.value))}/>
        <span>Password</span>
        <input type="password" value={password} onChange={evt => dispatch(setPassword(evt.target.value))}/>
        <button type="submit">Submit</button>
      </form>
    </div>
  )
}