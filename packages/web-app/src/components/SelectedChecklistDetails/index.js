import {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import * as _ from 'lodash';

import './index.css';

import {
  fetchSelectedChecklistTasks,
  selectedChecklistIdSelector,
  createSelectedChecklistTask,
  selectedChecklistTasksSelector,
  selectedChecklistTaskSelector
} from 'commons-client';

import {ChecklistTask} from "../ChecklistTask";

export function SelectedChecklistDetails() {
  const selectedChecklistId = useSelector(selectedChecklistIdSelector)
  const selectedChecklist = useSelector(selectedChecklistTaskSelector);
  const selectedChecklistTasks = useSelector(selectedChecklistTasksSelector)
  const dispatch = useDispatch();
  const [newTaskTitle, setNewTaskTitle] = useState("");
  const [taskCheckedState, setTaskCheckedState] = useState({});

  useEffect(() => {
    if (selectedChecklistId) {
      dispatch(fetchSelectedChecklistTasks())
      setTaskCheckedState({});
      setNewTaskTitle("");
    }
  }, [selectedChecklistId])

  const onSubmit = evt => {
    evt.preventDefault();

    dispatch(createSelectedChecklistTask({title: newTaskTitle}))
  }

  if (!selectedChecklist) {
    return (
      <div className={'selected-checklist-details none-selected'}>
        <span>Choose one of the checklists on the left side to start.</span>
      </div>
    )
  }

  const title = _.get(selectedChecklist, 'title');

  return (
    <div className={'selected-checklist-details'}>
      <div className={'title'}>
        <span>{title}</span>
      </div>
      <div className={'checklist-tasks-list'}>
        {selectedChecklistTasks.map(checklistTask => {
          const setChecked = v => setTaskCheckedState({...taskCheckedState, [checklistTask._id]: v});
          const checked = _.get(taskCheckedState, checklistTask._id, false);

          return <ChecklistTask checklistTask={checklistTask} checked={checked} setChecked={setChecked}/>
        })}
      </div>
      <button type="button" onClick={() => setTaskCheckedState({})}>Clear</button>
      <form className={'add-step-form'} onSubmit={onSubmit}>
        <input value={newTaskTitle} type={"text"} onChange={evt => setNewTaskTitle(evt.target.value)}/>
        <button type='submit'>Add step</button>
      </form>
    </div>
  );
}