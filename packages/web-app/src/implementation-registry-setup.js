import * as _ from "lodash";
import {ImplementationRegistry} from "commons-client";
import {useNavigate} from 'react-router';

export function setupImplementationRegistry() {
  ImplementationRegistry.registerImplementation('saveToPersistentStorage', function({key, data}) {
    const localStorageKey = `CHAPO_TODO_${_.upperCase(key)}`;

    if (!_.isString(data) && !data) {
      localStorage.removeItem(localStorageKey);

      return;
    }

    localStorage.setItem(localStorageKey, JSON.stringify(data));
  })

  ImplementationRegistry.registerImplementation('loadFromPersistentStorage', function({key}) {
    const localStorageKey = `CHAPO_TODO_${_.upperCase(key)}`;

    try {
      const stringifiedData = localStorage.getItem(localStorageKey);

      return JSON.parse(stringifiedData);

    } catch (e) {
      console.log(e)
      return null;
    }
  })

  const targetToUrlMap = {
    'Login': '/login',
    'Index': '/'
  }

  ImplementationRegistry.registerImplementation('getNavigateHook', function() {
    const navigate = useNavigate();

    return function({target}) {
      return navigate(targetToUrlMap[target]);
    }
  })
}
