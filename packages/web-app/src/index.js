import React from 'react';
import ReactDOM from 'react-dom';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {applyMiddleware} from 'redux';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import thunk from 'redux-thunk';

import {loadInitialStateFromPersistentStorage} from 'commons-client';
import {App, Login, Default} from './components';
import {setupImplementationRegistry} from "./implementation-registry-setup";

import './index.css';

setupImplementationRegistry();

loadInitialStateFromPersistentStorage().then(initialState => {
  function reducer(state = initialState, action) {
    return {
      ...state,
      ...action.payload
    }
  }

  const store = createStore(reducer, applyMiddleware(thunk));

  ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route index element={<App/>}/>
          <Route path="login" element={<Login/>}/>
          <Route path="*" element={<Default/>}/>
        </Routes>
      </BrowserRouter>
    </Provider>,
    document.getElementById('root')
  );
});


