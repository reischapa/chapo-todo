#!/usr/bin/env bash

sudo rm -rf packages/mobile-app/node_modules/commons-client
sudo rm -rf packages/web-app/node_modules/commons-client

mkdir -p packages/mobile-app/node_modules/commons-client
mkdir -p packages/web-app/node_modules/commons-client

sudo mount --bind packages/commons-client packages/mobile-app/node_modules/commons-client
sudo mount --bind packages/commons-client packages/web-app/node_modules/commons-client
